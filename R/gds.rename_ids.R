
##' convenient wrapper to \code{\link[gdsfmt]{add.gdsn}}
##'
##' just a convenient wrapper to a code{\link[gdsfmt]{add.gdsn}} function
##' @title rename sample.id in a gds file
##' @param gds_file character verctor with the name of the gds file
##' @param new character vector with the new ids to change
##' @return modify in the gds file the sample.id
##' @examples
##' \dontrun{
##' chr <- 1
##' gdsfile <- gdsfmt::openfn.gds(paste0("chr", chr, ".gds"), readonly = FALSE)
##' old <- gdsfmt::read.gdsn(gdsfmt::index.gdsn(gdsfile, "sample.id"))
##' print(paste( "chr", chr, "----", head(old,2)))
##'
##' extractids <- function(X){stringr::str_split(X, pattern = "_")[[1]][2]}
##' new <- plyr::laply(old, extractids)
##'
##' gdsfmt::add.gdsn(gdsfile, "sample.id", val = new, replace = TRUE, compress = "LZMA_RA", closezip = TRUE)
##'
##' closefn.gds(gdsfile)
##' }
##' @seealso \code{\link[SeqArray]{seqMerge}}
##'
##' @author person("Angel", "Martinez-Perez",
##' email = "angelmartinez@protonmail.com",
##' role = c("aut", "cre"),
##' comment = c(ORCID = "0000-0002-5934-1454"))
##' @importFrom gdsfmt add.gdsn
##' @importFrom gdsfmt read.gdsn
##'
##' @export
gds.rename_ids <- function(gds_file, new) {
    
    checkmate::assertFileExists(gds_file, access = "r", extension = "gds")
    gdsfile <- gdsfmt::openfn.gds(gds_file, readonly = FALSE)
    
    old_sample_id <- read.gdsn(index.gdsn(gdsfile, "sample.id"))

    if (!exists(new)) {
        extractids <- function(X){stringr::str_split(X, pattern = "_")[[1]][2]}
        new <- plyr::laply(old_sample_id, extractids)
    }

    gdsfmt::add.gdsn(gdsfile, "sample.id", val = new, replace = TRUE, compress = "LZMA_RA", closezip = TRUE)

    closefn.gds(gdsfile)
}
