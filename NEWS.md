# an9elannot 0.4.1

* Added a function to extract rsids from a region using dbSNP
* Added function to translate gene ids, and other to find the position of the genes

# an9elannot 0.4.0

# an9elannot 0.3.0

* Added function to launch GWA with GENESIS with dichotomic traits (function improved)
* Added functionality to `gds.gwa.xx.genesis` function to output the results in `gwasvcf` format
* Added function to match the annotatedDataFrame and the gds file
* Added function to make the rank_transform (is the wrong version of the normalize function)

# an9elannot 0.2.1

* Added function to launch GWA with GENESIS

# an9elannot 0.2.0

* Added function and test to get information about snps
* Added function and test to get information about snps in biomaRt

# an9elannot 0.1.0

* Added functions to filter vcf using vcftools and vcffilter
* Added functions to get info parameters directly from vcf files
* Added functions to merging gds files
* Added functions to make PCA with autosomals chr in gds file
* Added function to export vcf to gds format
* Added function to export vcf to .gen format (Oxford format)

# an9elannot 0.0.0.9000

* Added a `NEWS.md` file to track changes to the package.
* Added function `gds.pos.read` to load genotype from a `gds` file by position
