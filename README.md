
---
title: an9elannot package
author:
- name: Angel Martinez-Perez
- affiliation: Unit of Genomic of Complex Diseases
date: "Started: 2021-06-21"
---

Angel Martinez-Perez [![](https://info.orcid.org/wp-content/uploads/2019/11/orcid_24x24.png)](https://orcid.org/0000-0002-5934-1454)





# Install



```r
devtools::install_gitlab("an9el/an9elannot")
```


# Genetics

List of functions in this category:

* `tabix`
* `ranges.maker`
* `ranges.style`
* `vcf.read`
* `vcf.get.info` get some info from a vcf file, like MAF, R2, etc...
* `vcf.header`
* `vcf.filter` to filter variants in a vcf file for R2 and MAF
* `vcf.filter.subject` to make the same vcf file with less subjects
* `vcf2gds` to export vcf to gds files
* `gds.pos.read` to read specific genotypes from a gds in SEQArray format
* `gds.merging`to merge gds files
* `gds.PCA` to make PCA with the gds files
* `gdsSEQArray_2_gdsGWASTools` as a wrapper of the function `SeqArray::seqGDS2SNP`

## SNPs enrichment

In order to select genotypes directly from `.vcf` files I use
the package `VariantAnnotation`.   
I create some wrapper functions with the options I use more often.

1. First of all, a tabix index is needed, for made the tabix
of all the vcf files in one folder I create the function `tabix`.
1. To use the function `tabix` the files must be compressed with
bgzip algorithm. If not you can make like that using:
- (in terminal)
> for i in {1..22} ; do
>   gunzip chr${i}.vcf.gz"
> done
- (in R)
> for (i in 1:22) {
>  Rsamtools::bgzip(file = glue::glue("chr{i}.vcf"), dest = glue::glue("chr{i}.vcf.gz"))
>  unlink(glue::glue("chr{i}.vcf"))
>  VariantAnnotation::indexVcf(glue::glue("chr{i}.vcf.gz"))
> }
1. Then to create a query first we need to create a `GRanges`
object with the information about the chromosome the start and the
end of the positions to query, the reference used and possibly the
names of these regions. For example:


```r
rsAnges <- an9elannot::ranges.maker("6",
	ref = "hg38",
	start = as.integer(c(1000187, 1000213, 1000224)),
	names = c("rs920868899", "rs1323820649", "rs545711931"))
rsAnges <- an9elannot::ranges.style(rsAnges, ranges.style = "addchr")
```


With this `GRanges` object defined you can also enrich these SNPs,
if they have name or rs. This will provide more information about
the position of these SNPs and the consequence of its functions.
We will use these packages:

* TxDb.Hsapiens.UCSC.hg38.knownGene
* TxDb.Hsapiens.UCSC.hg19.knownGene
* PolyPhen.Hsapiens.dbSNP131
* SIFT.Hsapiens.dbSNP132.db
* rsnps
* SNPlocs.Hsapiens.dbSNP151.GRCh38

For example:



```r
library(TxDb.Hsapiens.UCSC.hg38.knownGene)
library(PolyPhen.Hsapiens.dbSNP131)
txdb <- TxDb.Hsapiens.UCSC.hg38.knownGene
loc <- locateVariants(rsAnges, txdb, IntronVariants())
pp <- select(PolyPhen.Hsapiens.dbSNP131, keys = names(rsAnges),
   cols = c("TRAININGSET", "PREDICTION", "PPH2PROB"))
snps2 <- rsnps::ncbi_snp_query(snps = names(rsAnges))
```


## get the `rs` name of snps given `chr:position`

Example in the `GRCh38.p7` reference (if this is not the actual
reference, please use the proper `SNPlocs.Hsapiens.dbSNPxxx.GRChxx`
package):



```r
libray(SNPlocs.Hsapiens.dbSNP151.GRCh38)
snps <- SNPlocs.Hsapiens.dbSNP151.GRCh38
rsAnges3 <- an9elannot::ranges.maker(
    "6",
    ref = "GRCh38.p7",
    start = as.integer(c(1000187, 1000213, 1000224)))
rsIDs <- snpsByOverlaps(snps, rsAnges3)
rsIDs@elementMetadata$RefSNP_id
## if all the snps are returned then we can update our `GRanges` object
if (length(rsIDs) == length(rsAnges3)) {
    rsAnges3@elementMetadata <- rsIDs@elementMetadata
    names(rsAnges3@ranges) <- rsIDs@elementMetadata$RefSNP_id
}
```


## vcf summary

First we can explore the content of the vcfFile with:



```r
aux <- an9elannot::vcf.header(vcfFile)
## To list the first 8 samplesID:
sampl8 <- samples(aux)[1:8]
## To see the different formats of genotypes that are in the vcfFile:
geno(aux)
## To see the info of the different genotypes:
info(aux)
```


## vcf query

Given the `GRanges` object defined above we can extract some genotypes of some samples with:



```r
res <- an9elannot::vcf.read(
              "chr6.vcf",
              rsAnges,
              samples = sampl8,
              genoType = "DS")
```


This will return a list with three slots:

* The map of the genotypes
* The info of the genotypes
* The values of the genotypes for the samples selected
(notice that you can select different format for this output with the `genoType` parameter).


# SESSION INFO


<details closed>
<summary> <span title='Clik to Expand'> Current session info </span> </summary>

```r

─ Session info ────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
 setting  value
 version  R version 4.4.2 (2024-10-31)
 os       Ubuntu 24.04.2 LTS
 system   x86_64, linux-gnu
 ui       X11
 language (EN)
 collate  es_ES.UTF-8
 ctype    es_ES.UTF-8
 tz       Europe/Madrid
 date     2025-02-20
 pandoc   3.1.3 @ /usr/bin/ (via rmarkdown)
 quarto   1.6.40 @ /usr/local/bin/quarto

─ Packages ────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
 ! package              * version    date (UTC) lib source
   abind                  1.4-8      2024-09-12 [2] CRAN (R 4.4.1)
 P an9elannot           * 0.4.1      2025-02-19 [?] gitlab (an9el/an9elannot@cf73000)
   an9elutils           * 0.7.1      2025-01-28 [2] gitlab (an9el/an9elutils@13806c4)
   AnnotationDbi          1.66.0     2024-05-01 [2] Bioconductor 3.19 (R 4.4.0)
   askpass                1.2.1      2024-10-04 [2] CRAN (R 4.4.1)
   backports              1.5.0      2024-05-23 [2] CRAN (R 4.4.0)
   base64enc              0.1-3      2015-07-28 [2] CRAN (R 4.3.1)
   bibtex                 0.5.1      2023-01-26 [2] CRAN (R 4.3.1)
   Biobase                2.64.0     2024-04-30 [2] Bioconductor 3.19 (R 4.4.0)
   BiocFileCache          2.12.0     2024-04-30 [2] Bioconductor 3.19 (R 4.4.0)
   BiocGenerics           0.50.0     2024-04-30 [2] Bioconductor 3.19 (R 4.4.0)
   BiocIO                 1.14.0     2024-04-30 [2] Bioconductor 3.19 (R 4.4.0)
   BiocParallel           1.38.0     2024-04-30 [2] Bioconductor 3.19 (R 4.4.0)
   biomaRt                2.60.1     2024-06-26 [2] Bioconductor 3.19 (R 4.4.1)
   Biostrings             2.72.1     2024-06-02 [2] Bioconductor 3.19 (R 4.4.0)
   bit                    4.5.0.1    2024-12-03 [2] CRAN (R 4.4.2)
   bit64                  4.6.0-1    2025-01-16 [2] CRAN (R 4.4.2)
   bitops                 1.0-9      2024-10-03 [2] CRAN (R 4.4.1)
   blob                   1.2.4      2023-03-17 [2] CRAN (R 4.3.1)
   boot                   1.3-31     2024-08-28 [4] CRAN (R 4.4.2)
   broom                * 1.0.7      2024-09-26 [2] CRAN (R 4.4.1)
   BSgenome               1.72.0     2024-04-30 [2] Bioconductor 3.19 (R 4.4.0)
   cachem                 1.1.0      2024-05-16 [2] CRAN (R 4.4.0)
   caret                  7.0-1      2024-12-10 [2] CRAN (R 4.4.2)
   checkmate              2.3.2      2024-07-29 [2] CRAN (R 4.4.1)
   class                  7.3-23     2025-01-01 [4] CRAN (R 4.4.2)
   cli                    3.6.4      2025-02-13 [2] CRAN (R 4.4.2)
   clipr                  0.8.0      2022-02-22 [2] CRAN (R 4.3.1)
   coda                   0.19-4.1   2024-01-31 [2] CRAN (R 4.3.1)
   codetools              0.2-20     2024-03-31 [2] CRAN (R 4.3.1)
   collapse               2.0.19     2025-01-09 [2] CRAN (R 4.4.2)
   colorspace             2.1-1      2024-07-26 [2] CRAN (R 4.4.1)
   commonmark             1.9.2      2024-10-04 [2] CRAN (R 4.4.1)
   crayon                 1.5.3      2024-06-20 [2] CRAN (R 4.4.1)
   crul                   1.5.0      2024-07-19 [2] CRAN (R 4.4.1)
   curl                   6.2.1      2025-02-19 [2] CRAN (R 4.4.2)
   data.table           * 1.16.4     2024-12-06 [2] CRAN (R 4.4.2)
   DataExplorer         * 0.8.3      2024-01-24 [2] CRAN (R 4.3.1)
   datawizard             1.0.0      2025-01-10 [2] CRAN (R 4.4.2)
   DBI                    1.2.3      2024-06-02 [2] CRAN (R 4.4.0)
   dbplyr                 2.5.0      2024-03-19 [2] CRAN (R 4.3.1)
   DelayedArray           0.30.1     2024-05-07 [2] Bioconductor 3.19 (R 4.4.0)
   desc                   1.4.3      2023-12-10 [2] CRAN (R 4.3.1)
   details              * 0.4.0      2025-02-09 [2] CRAN (R 4.4.2)
   devtools               2.4.5      2022-10-11 [2] CRAN (R 4.3.1)
   digest                 0.6.37     2024-08-19 [2] CRAN (R 4.4.1)
   DNAcopy                1.78.0     2024-04-30 [2] Bioconductor 3.19 (R 4.4.0)
   dplyr                * 1.1.4      2023-11-17 [2] CRAN (R 4.3.1)
   DT                   * 0.33       2024-04-04 [2] CRAN (R 4.3.1)
   ellipsis               0.3.2      2021-04-29 [2] CRAN (R 4.3.1)
   emmeans                1.10.7     2025-01-31 [2] CRAN (R 4.4.2)
   estimability           1.5.1      2024-05-12 [2] CRAN (R 4.4.0)
   evaluate               1.0.3      2025-01-10 [1] CRAN (R 4.4.2)
   fastmap                1.2.0      2024-05-15 [2] CRAN (R 4.4.0)
   filelock               1.0.3      2023-12-11 [2] CRAN (R 4.3.1)
   forcats              * 1.0.0      2023-01-29 [2] CRAN (R 4.3.1)
   foreach                1.5.2      2022-02-02 [2] CRAN (R 4.3.1)
   formula.tools          1.7.1      2018-03-01 [2] CRAN (R 4.3.1)
   fs                     1.6.5      2024-10-30 [2] CRAN (R 4.4.2)
   future                 1.34.0     2024-07-29 [2] CRAN (R 4.4.1)
   future.apply           1.11.3     2024-10-27 [2] CRAN (R 4.4.1)
   gdsfmt                 1.40.0     2024-04-30 [1] Bioconductor 3.19 (R 4.4.0)
   generics               0.1.3      2022-07-05 [2] CRAN (R 4.3.1)
   GENESIS                2.34.0     2024-04-30 [2] Bioconductor 3.19 (R 4.4.0)
   GenomeInfoDb           1.40.1     2024-05-24 [2] Bioconductor 3.19 (R 4.4.0)
   GenomeInfoDbData       1.2.12     2024-05-15 [2] Bioconductor
   GenomicAlignments      1.40.0     2024-04-30 [2] Bioconductor 3.19 (R 4.4.0)
   GenomicFeatures        1.56.0     2024-04-30 [2] Bioconductor 3.19 (R 4.4.0)
   GenomicRanges          1.56.2     2024-10-09 [2] Bioconductor 3.19 (R 4.4.1)
   ggplot2              * 3.5.1      2024-04-23 [2] CRAN (R 4.3.1)
   glmnet                 4.1-8      2023-08-22 [2] CRAN (R 4.3.1)
   globals                0.16.3     2024-03-08 [2] CRAN (R 4.3.1)
   glue                 * 1.8.0      2024-09-30 [2] CRAN (R 4.4.1)
   gower                  1.0.2      2024-12-17 [2] CRAN (R 4.4.2)
   gridExtra              2.3        2017-09-09 [2] CRAN (R 4.3.1)
   gtable                 0.3.6      2024-10-25 [2] CRAN (R 4.4.1)
   GWASExactHW            1.2        2024-03-12 [2] CRAN (R 4.3.1)
   GWASTools              1.50.0     2024-04-30 [2] Bioconductor 3.19 (R 4.4.0)
   gwasvcf                0.1.2      2023-09-06 [2] Github (mrcieu/gwasvcf@477b365)
   hardhat                1.4.1      2025-01-31 [2] CRAN (R 4.4.2)
   here                 * 1.0.1      2020-12-13 [2] CRAN (R 4.4.1)
   hms                    1.1.3      2023-03-21 [2] CRAN (R 4.3.1)
   htmltools              0.5.8.1    2024-04-04 [2] CRAN (R 4.3.1)
   htmlwidgets          * 1.6.4      2023-12-06 [2] CRAN (R 4.3.1)
   httpcode               0.3.0      2020-04-10 [2] CRAN (R 4.3.1)
   httpuv                 1.6.15     2024-03-26 [2] CRAN (R 4.3.1)
   httr                   1.4.7      2023-08-15 [2] CRAN (R 4.3.1)
   httr2                  1.1.0      2025-01-18 [2] CRAN (R 4.4.2)
   igraph                 2.1.4      2025-01-23 [2] CRAN (R 4.4.2)
   insight                1.0.2      2025-02-06 [2] CRAN (R 4.4.2)
   ipred                  0.9-15     2024-07-18 [2] CRAN (R 4.4.1)
   IRanges                2.38.1     2024-07-03 [2] Bioconductor 3.19 (R 4.4.1)
   iterators              1.0.14     2022-02-05 [2] CRAN (R 4.3.1)
   jomo                   2.7-6      2023-04-15 [2] CRAN (R 4.3.1)
   jsonlite               1.9.0      2025-02-19 [2] CRAN (R 4.4.2)
   kableExtra           * 1.4.0      2024-01-24 [2] CRAN (R 4.3.1)
   KEGGREST               1.44.1     2024-06-19 [2] Bioconductor 3.19 (R 4.4.1)
   knitcitations        * 1.0.12     2021-01-10 [2] CRAN (R 4.3.1)
   knitr                * 1.49       2024-11-08 [2] CRAN (R 4.4.2)
   later                  1.4.1      2024-11-27 [2] CRAN (R 4.4.2)
   lattice              * 0.22-6     2024-03-20 [2] CRAN (R 4.3.1)
   lava                   1.8.1      2025-01-12 [2] CRAN (R 4.4.2)
   lifecycle              1.0.4      2023-11-07 [2] CRAN (R 4.3.1)
   listenv                0.9.1      2024-01-29 [2] CRAN (R 4.3.1)
   lme4                   1.1-36     2025-01-11 [2] CRAN (R 4.4.2)
   lmtest                 0.9-40     2022-03-21 [2] CRAN (R 4.3.1)
   logistf                1.26.0     2023-08-18 [2] CRAN (R 4.3.1)
   lubridate            * 1.9.4      2024-12-08 [2] CRAN (R 4.4.2)
   magick                 2.8.5      2024-09-20 [2] CRAN (R 4.4.1)
   magrittr             * 2.0.3      2022-03-30 [2] CRAN (R 4.3.1)
   MASS                   7.3-64     2025-01-04 [2] CRAN (R 4.4.2)
   Matrix                 1.7-2      2025-01-23 [2] CRAN (R 4.4.2)
   MatrixGenerics         1.16.0     2024-04-30 [2] Bioconductor 3.19 (R 4.4.0)
   MatrixModels           0.5-3      2023-11-06 [2] CRAN (R 4.3.1)
   matrixStats            1.5.0      2025-01-07 [2] CRAN (R 4.4.2)
   memoise                2.0.1      2021-11-26 [2] CRAN (R 4.3.1)
   mgcv                   1.9-1      2023-12-21 [4] CRAN (R 4.3.2)
   mice                   3.17.0     2024-11-27 [2] CRAN (R 4.4.2)
   mime                   0.12       2021-09-28 [2] CRAN (R 4.3.1)
   miniUI                 0.1.1.1    2018-05-18 [2] CRAN (R 4.3.1)
   minqa                  1.2.8      2024-08-17 [2] CRAN (R 4.4.1)
   mitml                  0.4-5      2023-03-08 [2] CRAN (R 4.3.1)
   ModelMetrics           1.2.2.2    2020-03-17 [2] CRAN (R 4.3.1)
   multcomp               1.4-28     2025-01-29 [2] CRAN (R 4.4.2)
   munsell                0.5.1      2024-04-01 [2] CRAN (R 4.3.1)
   mvtnorm                1.3-3      2025-01-10 [2] CRAN (R 4.4.2)
   networkD3              0.4        2017-03-18 [2] CRAN (R 4.3.1)
   nlme                   3.1-167    2025-01-27 [4] CRAN (R 4.4.2)
   nloptr                 2.1.1      2024-06-25 [2] CRAN (R 4.4.1)
   nnet                   7.3-20     2025-01-01 [4] CRAN (R 4.4.2)
   openssl                2.3.2      2025-02-03 [2] CRAN (R 4.4.2)
   operator.tools         1.6.3      2017-02-28 [2] CRAN (R 4.3.1)
   pacman               * 0.5.1      2019-03-11 [2] CRAN (R 4.3.1)
   pan                    1.9        2023-08-21 [2] CRAN (R 4.3.1)
   pander                 0.6.5      2022-03-18 [2] CRAN (R 4.3.1)
   parallelly             1.42.0     2025-01-30 [2] CRAN (R 4.4.2)
   pillar                 1.10.1     2025-01-07 [2] CRAN (R 4.4.2)
   pkgbuild               1.4.6      2025-01-16 [2] CRAN (R 4.4.2)
   pkgconfig              2.0.3      2019-09-22 [2] CRAN (R 4.3.1)
   pkgload                1.4.0      2024-06-28 [2] CRAN (R 4.4.1)
   plyr                 * 1.8.9      2023-10-02 [2] CRAN (R 4.3.1)
   png                    0.1-8      2022-11-29 [2] CRAN (R 4.3.1)
   ppsr                 * 0.0.5      2024-02-18 [2] CRAN (R 4.3.1)
   prettyunits            1.2.0      2023-09-24 [2] CRAN (R 4.3.1)
   pROC                   1.18.5     2023-11-01 [2] CRAN (R 4.3.1)
   prodlim                2024.06.25 2024-06-24 [2] CRAN (R 4.4.1)
   profvis                0.4.0      2024-09-20 [2] CRAN (R 4.4.1)
   progress               1.2.3      2023-12-06 [2] CRAN (R 4.3.1)
   promises               1.3.2      2024-11-28 [2] CRAN (R 4.4.2)
   pryr                   0.1.6      2023-01-17 [2] CRAN (R 4.3.1)
   purrr                * 1.0.4      2025-02-05 [2] CRAN (R 4.4.2)
   quantreg               6.00       2025-01-29 [2] CRAN (R 4.4.2)
   quantsmooth            1.70.0     2024-04-30 [2] Bioconductor 3.19 (R 4.4.0)
   R6                     2.6.1      2025-02-15 [2] CRAN (R 4.4.2)
   rappdirs               0.3.3      2021-01-31 [2] CRAN (R 4.3.1)
   rapportools            1.1        2022-03-22 [2] CRAN (R 4.3.1)
   rbibutils              2.3        2024-10-04 [2] CRAN (R 4.4.1)
   Rcpp                   1.0.14     2025-01-12 [2] CRAN (R 4.4.2)
   RCurl                  1.98-1.16  2024-07-11 [2] CRAN (R 4.4.1)
   Rdpack                 2.6.2      2024-11-15 [2] CRAN (R 4.4.2)
   readr                * 2.1.5      2024-01-10 [2] CRAN (R 4.3.1)
   recipes                1.1.1      2025-02-12 [2] CRAN (R 4.4.2)
   RefManageR             1.4.0      2022-09-30 [2] CRAN (R 4.3.1)
   reformulas             0.4.0      2024-11-03 [2] CRAN (R 4.4.2)
   remotes                2.5.0      2024-03-17 [2] CRAN (R 4.3.1)
   reshape2               1.4.4      2020-04-09 [2] CRAN (R 4.3.1)
   restfulr               0.0.15     2022-06-16 [2] CRAN (R 4.3.1)
   rjson                  0.2.23     2024-09-16 [2] CRAN (R 4.4.1)
   rlang                  1.1.5      2025-01-17 [2] CRAN (R 4.4.2)
   rlog                   0.1.0      2021-02-24 [2] CRAN (R 4.3.1)
   rmarkdown            * 2.29       2024-11-04 [2] CRAN (R 4.4.2)
   roxygen2               7.3.2      2024-06-28 [2] CRAN (R 4.4.1)
   rpart                  4.1.24     2025-01-07 [4] CRAN (R 4.4.2)
   rprojroot              2.0.4      2023-11-05 [2] CRAN (R 4.3.1)
   Rsamtools              2.20.0     2024-04-30 [2] Bioconductor 3.19 (R 4.4.0)
   rsnps                  0.6.0      2023-06-30 [2] CRAN (R 4.2.2)
   RSQLite                2.3.9      2024-12-03 [2] CRAN (R 4.4.2)
   rstudioapi             0.17.1     2024-10-22 [2] CRAN (R 4.4.1)
   rtracklayer            1.64.0     2024-04-30 [2] Bioconductor 3.19 (R 4.4.0)
   S4Arrays               1.4.1      2024-05-20 [2] Bioconductor 3.19 (R 4.4.0)
   S4Vectors              0.42.1     2024-07-03 [2] Bioconductor 3.19 (R 4.4.1)
   sandwich               3.1-1      2024-09-15 [2] CRAN (R 4.4.1)
   scales                 1.3.0      2023-11-28 [2] CRAN (R 4.3.1)
   SeqArray               1.44.3     2024-10-02 [2] Bioconductor 3.19 (R 4.4.1)
   SeqVarTools            1.42.0     2024-04-30 [2] Bioconductor 3.19 (R 4.4.0)
   sessioninfo            1.2.3      2025-02-05 [2] CRAN (R 4.4.2)
   shape                  1.4.6.1    2024-02-23 [2] CRAN (R 4.3.1)
   shiny                  1.10.0     2024-12-14 [2] CRAN (R 4.4.2)
   SNPRelate              1.38.1     2024-10-02 [2] Bioconductor 3.19 (R 4.4.1)
   SparseArray            1.4.8      2024-05-24 [2] Bioconductor 3.19 (R 4.4.0)
   SparseM                1.84-2     2024-07-17 [2] CRAN (R 4.4.1)
   stringi                1.8.4      2024-05-06 [2] CRAN (R 4.3.3)
   stringr              * 1.5.1      2023-11-14 [2] CRAN (R 4.3.1)
   SummarizedExperiment   1.34.0     2024-05-01 [2] Bioconductor 3.19 (R 4.4.0)
   summarytools         * 1.0.1      2022-05-20 [2] CRAN (R 4.3.1)
   survival               3.8-3      2024-12-17 [4] CRAN (R 4.4.2)
   svglite                2.1.3      2023-12-08 [2] CRAN (R 4.3.1)
   systemfonts            1.2.1      2025-01-20 [2] CRAN (R 4.4.2)
   TH.data                1.1-3      2025-01-17 [2] CRAN (R 4.4.2)
   tibble               * 3.2.1      2023-03-20 [2] CRAN (R 4.3.1)
   tidyr                * 1.3.1.9000 2024-11-13 [2] Github (tidyverse/tidyr@3a9a93c)
   tidyselect             1.2.1      2024-03-11 [2] CRAN (R 4.3.1)
   tidyverse            * 2.0.0      2023-02-22 [2] CRAN (R 4.3.1)
   timechange             0.3.0      2024-01-18 [2] CRAN (R 4.3.1)
   timeDate               4041.110   2024-09-22 [2] CRAN (R 4.4.1)
   tzdb                   0.4.0      2023-05-12 [2] CRAN (R 4.3.1)
   UCSC.utils             1.0.0      2024-04-30 [2] Bioconductor 3.19 (R 4.4.0)
   urlchecker             1.0.1      2021-11-30 [2] CRAN (R 4.3.1)
   usethis                3.1.0      2024-11-26 [2] CRAN (R 4.4.2)
   VariantAnnotation      1.50.0     2024-04-30 [2] Bioconductor 3.19 (R 4.4.0)
   vctrs                  0.6.5      2023-12-01 [2] CRAN (R 4.3.1)
   viridis                0.6.5      2024-01-29 [2] CRAN (R 4.3.1)
   viridisLite            0.4.2      2023-05-02 [2] CRAN (R 4.3.1)
   visNetwork           * 2.1.2      2022-09-29 [2] CRAN (R 4.3.1)
   vroom                  1.6.5      2023-12-05 [2] CRAN (R 4.3.1)
   webshot              * 0.5.5      2023-06-26 [2] CRAN (R 4.4.1)
   withr                  3.0.2      2024-10-28 [2] CRAN (R 4.4.2)
   xfun                   0.50       2025-01-07 [1] CRAN (R 4.4.2)
   XML                    3.99-0.18  2025-01-01 [2] CRAN (R 4.4.2)
   xml2                   1.3.6      2023-12-04 [2] CRAN (R 4.3.1)
   xtable                 1.8-4      2019-04-21 [2] CRAN (R 4.3.1)
   XVector                0.44.0     2024-04-30 [2] Bioconductor 3.19 (R 4.4.0)
   yaml                   2.3.10     2024-07-26 [2] CRAN (R 4.4.1)
   zlibbioc               1.50.0     2024-04-30 [2] Bioconductor 3.19 (R 4.4.0)
   zoo                    1.8-12     2023-04-13 [2] CRAN (R 4.3.1)

 [1] /home/amartinezp/R/x86_64-pc-linux-gnu-library/4.4
 [2] /usr/local/lib/R/site-library
 [3] /usr/lib/R/site-library
 [4] /usr/lib/R/library

 * ── Packages attached to the search path.
 P ── Loaded and on-disk path mismatch.

───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────

```

</details>
<br>

